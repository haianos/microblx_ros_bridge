#include "byte_stream_decoder.hpp"
#include <iostream>
#include <assert.h>

/* edit and uncomment this:
 * UBX_MODULE_LICENSE_SPDX(GPL-2.0+)
 */

/* hexdump a buffer */
static void hexdump(unsigned char *buf, unsigned long index, unsigned long width)
{
	unsigned long i, fill;

	for (i=0;i<index;i++) { printf("%02x ", buf[i]); } /* dump data */
	for (fill=index; fill<width; fill++) printf(" ");  /* pad on right */

	printf(": ");

	/* add ascii repesentation */
	for (i=0; i<index; i++) {
		if (buf[i] < 32) printf(".");
		else printf("%c", buf[i]);
	}
	printf("\n");
}



/* define a structure for holding the block local state. By assigning an
 * instance of this struct to the block private_data pointer (see init), this
 * information becomes accessible within the hook functions.
 */
struct byte_stream_decoder_info
{
        /* add custom block local data here */

        /* this is to have fast access to ports for reading and writing, without
         * needing a hash table lookup */
        struct byte_stream_decoder_port_cache ports;
};

/* init */
int byte_stream_decoder_init(ubx_block_t *b)
{
        int ret = -1;
        struct byte_stream_decoder_info *inf;

        /* allocate memory for the block local state */
        if ((inf = (struct byte_stream_decoder_info*)calloc(1, sizeof(struct byte_stream_decoder_info)))==NULL) {
                ERR("byte_stream_decoder: failed to alloc memory");
                ret=EOUTOFMEM;
                goto out;
        }
        b->private_data=inf;
        update_port_cache(b, &inf->ports);
        ret=0;
out:
        return ret;
}

/* start */
int byte_stream_decoder_start(ubx_block_t *b)
{
        /* struct byte_stream_decoder_info *inf = (struct byte_stream_decoder_info*) b->private_data; */
        int ret = 0;
        return ret;
}

/* stop */
void byte_stream_decoder_stop(ubx_block_t *b)
{
        /* struct byte_stream_decoder_info *inf = (struct byte_stream_decoder_info*) b->private_data; */
}

/* cleanup */
void byte_stream_decoder_cleanup(ubx_block_t *b)
{
        free(b->private_data);
}

/* step */
void byte_stream_decoder_step(ubx_block_t *b)
{

        struct byte_stream_decoder_info *inf = (struct byte_stream_decoder_info*) b->private_data;
		std::cout << "step @ decoder " << std::endl;

		/* read data from port */
		ubx_port_t* port = inf->ports.byte_stream;
		assert(port != 0);

		ubx_data_t val;
		checktype(port->block->ni, port->in_type, "unsigned char", port->name, 1);
		val.type=port->in_type;
		val.len = 1; // actually this value will be ignored.
		__port_read(port, &val);

		std::cout << " Retrived data_size = " << data_size(&val) << " bytes. Data = " << std::endl;
		hexdump((unsigned char*)val.data, val.len, 16);
		std::cout << std::endl;

}

