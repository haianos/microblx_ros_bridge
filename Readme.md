Microblx ROS Bridge
=========
> Collection of interactive and computational micro function blocks for the 
microblx framework and its integration with the ROS framework.

Dependencies
-----------

It requires 

* ROS (roscpp library)
* microblocks (dev branch from https://github.com/haianos/microblx/tree/dev)

Tested on:
* Fuerte

Installation
--------------
* First, be sure to have ROS installed and set up properly its environment variables (ROS_ROOT,ROS_WORKSPACE,...)

* Second, set up the UBX_ROOT environment variable
```sh
export UBX_ROOT=[path-to-microblx]
```
* Then, download the source in your favorite directory by typing
```sh
git clone [git-repo-url] dillinger
mkdir build && cd build
cmake ..
make
```

Quickstart
-----------

```
source $UBX_ROOT/env.sh
./run_test.sh
```

In a new terminal window, start the ros master:
```
roscore
```

Use the web interface (http://localhost:8888/) to inspect and start the example.


Design
--------

The ROS bridge consists of essentially two iblocks: a publisher block and a subscriber block. 
In its current verison the bridge is most useful to be used in a microblox->ROS->microblox setting. 

* Publisher iblock: Whenever the `write(...)` function is invoked the date will be converted to (a rather generic) 
  `std_msg::ByteMultiArray` ROS message and published on a ROS topic as specified by the `topic_name`
  parameter.

* Subscriber iblock: The subscriber contains a thread (`ros::AsyncSpinner`) to wait for arriving messages 
  on a topic as defined by `topic_name`. Whenever the `read(...)` function of this iblock is envoked the data 
  of last recived ROS message is returned.
  Optionally a list of obervers can be attached via the `trigger_blocks` configuation parameter. In this
  case the `step(...)` functions are tiggerend on arrival of fresh data.
  NOTE: The `trigger_blocks` configuration is of the same type a used in the ptrig standard trigger block, 
  for convenience.     


![ubx_ros_bridge.png](doc/figs/ubx_ros_bridge.png)

TODO
---

* Indicate the ubx type in the ROS message.
* Support for conversion to existing ROS messages.

License
---
GPLv2

Authors
-----
Enea Scioni,
Sebastian Blumenthal
