/*
 * ROS Bridge iblock - Publisher
 * Authors: Enea Scioni, Sebastian Blumenthal
 * email: enea dot scioni at unife dot it
 *        enea dot scioni at kuleuven dot be
 *        sebastian dot blumenthal at kuleuven dot be
 */

#include <ros/ros.h>
#include <std_msgs/ByteMultiArray.h>
#include "ubx.h"
#include <stdio.h>

/* hexdump a buffer */
static void hexdump(unsigned char *buf, unsigned long index, unsigned long width)
{
	unsigned long i, fill;

	for (i=0;i<index;i++) { printf("%02x ", buf[i]); } /* dump data */
	for (fill=index; fill<width; fill++) printf(" ");  /* pad on right */

	printf(": ");

	/* add ascii repesentation */
	for (i=0; i<index; i++) {
		if (buf[i] < 32) printf(".");
		else printf("%c", buf[i]);
	}
	printf("\n");
}

// struct PublisherHandle
// {
//   std::string topic_name;
//   std_msgs::String ros_msg;
//   ros::NodeHandle* nh;
//   ros::Publisher* pub;
// };

struct publisher_info {
  std_msgs::ByteMultiArray* ros_msg;
  ros::NodeHandle* nh;
  ros::Publisher* pub;
  publisher_info()
    : ros_msg(new std_msgs::ByteMultiArray())
  {
  }
};

char ros_publisher_meta[] =
  "{ doc='ROS bridge interaction block',"
  "  license='MIT',"
  "  real-time=false,"
  "}";
  
ubx_config_t ros_publisher_config[] = {
        { .name="topic_name", .type_name = "char" },
        { NULL },
};

ubx_port_t ros_publisher_ports[] = {
        { .name="byte_stream", .attrs=PORT_DIR_IN, .in_type_name="unsigned char" },
//         { .name="bar", .attrs=PORT_DIR_OUT, .out_type_name="unsigned int" },
        { NULL },
};

static void publish_data(ubx_block_t *i, ubx_data_t* data) 
{

  const char* datatypename = get_typename(data);
  printf("hexdump (%s): ", (datatypename!=0) ? datatypename : "unknown");
  hexdump((unsigned char*)data->data, data_size(data), 16);

  /* prepare ROS message */
  std_msgs::ByteMultiArray msg;
  msg.layout.data_offset = 0;
  msg.layout.dim.resize(1);
  msg.layout.dim[0].label = "raw byte data stream";
  msg.layout.dim[0].size = data_size(data);//4;//4;//data->len;
  msg.layout.dim[0].stride = msg.layout.dim[0].size; //?

  /* copy the byte data to ROS message*/
  msg.data.resize(msg.layout.dim[0].size);
//  msg.data[0] = 0xDE; //DEAFBEAF dummy message
//  msg.data[1] = 0xAF;
//  msg.data[2] = 0xBE;
//  msg.data[3] = 0xAF;
  for (int i = 0; i < msg.layout.dim[0].size; ++i) {
	  msg.data[i] = ((unsigned char *)(data->data))[i];
  }

  /* publish the ROS message*/
  struct publisher_info *inf=(publisher_info *)i->private_data;
  inf->pub->publish(msg);
}

extern "C" {
  bool ros_master_check() {
    return ros::master::check();
  }
}

static int ros_publisher_init(ubx_block_t *b)
{
  int ret=-1;
  if(!ros::isInitialized())
  {
    ERR("ros init");
    int argc=0;
    char ** argv=NULL;
    ros::init(argc,argv,"microblx",ros::init_options::AnonymousName);
    if(ros_master_check())
      ros::start();
    else
    {
      ERR("'roscore' is not running: no ROS functions will be available.");
      ros::shutdown();
      goto out;
    }
  }
  else
    ERR("skip");
  
//   static ros::AsyncSpinner spinner(1); // Use 1 threads
//   spinner.start();

  /* POINTER VERSION
  char* tname; unsigned int len;
  tname = (char *) ubx_config_get_data_ptr(b, "topic_name", &len);
  if(strncmp(tname, "", len)==0) 
  {
    ERR("config topic_name unset");
    goto out;
  }
//     nh = new ros::NodeHandle("~");
  nh = new ros::NodeHandle();
//     pub = new ros::Publisher();
  if(pub==NULL)
    pub = new ros::Publisher(nh->advertise<std_msgs::String>(tname,10));
  topic_name = std::string(tname);
  */
  
  char* tname; unsigned int len;
  tname = (char *) ubx_config_get_data_ptr(b, "topic_name", &len);
  bool isok;
  if(strncmp(tname, "", len)==0) 
  {
    ERR("config topic_name unset");
    goto out;
  }

  struct publisher_info *inf;
  /*allocate internal block data*/
  if((inf=(struct publisher_info*)calloc(1,sizeof(struct publisher_info)))==NULL) {
    ERR("failed to allocate publisher_info");
    goto out;
  }
  
  b->private_data=inf;
  
  inf->nh = new ros::NodeHandle();
  isok = inf->nh->ok();//SEG_FAULT HERE
  std::cout << isok << std::endl;
  inf->pub = new ros::Publisher(inf->nh->advertise<std_msgs::ByteMultiArray>(tname,10));
  
  
      ret=0;
      goto out;
// out_free:
//       free(b->private_data);
out:
      return ret;
}

static void ros_publisher_cleanup(ubx_block_t *b) {
    ERR("cleaningup");
  struct publisher_info* inf = (struct publisher_info*)(b->private_data);
  inf->pub->shutdown();
  if(ros::isInitialized())
    ros::shutdown();
  delete inf->pub;
  delete inf->nh;
  free((struct publisher_info*) b->private_data);
}

/* put everything together */
ubx_block_t ros_publisher_comp = {
        .name = "rosbridge/publisher",
        .type = BLOCK_TYPE_INTERACTION,
        .meta_data = ros_publisher_meta,
        
        .configs = ros_publisher_config,
        
        /* ops */
        .init = ros_publisher_init,
        .write = publish_data,
        .cleanup = ros_publisher_cleanup,
};

static int ros_publisher_mod_init(ubx_node_info_t* ni)
{
//         DBG(" ");
//         if(!ros::isInitialized()){
//         int argc=0;
//         char ** argv=NULL;
//         ros::init(argc,argv,"microblx",ros::init_options::AnonymousName);
//         if(ros_master_check())
//           ros::start();
//         else
//         {
//           DBG("'roscore' is not running: no ROS functions will be available.");
//           ros::shutdown();
//           return true;
//       }
//     }
//     static ros::AsyncSpinner spinner(1); // Use 1 threads
//     spinner.start();
//     
//     char* tname; unsigned int len;
//     tname = (char *) ubx_config_get_data_ptr(ni, "topic_name", &len);
//     if(strncmp(tname, "", len)==0) 
//     {
//       ERR("config topic_name unset");
//       return true;
//     }
// //     nh = new ros::NodeHandle("~");
//     nh = new ros::NodeHandle();
// //     pub = new ros::Publisher();
//     pub = new ros::Publisher(nh->advertise<std_msgs::String>(tname,10));
//     log(Info)<<"ROS node spinner started"<<endlog();
ERR("mod_init");
        return ubx_block_register(ni, &ros_publisher_comp);
}

static void ros_publisher_mod_cleanup(ubx_node_info_t *ni)
{
  ERR("modclean");
//         DBG(" ");
//         delete pub;
//         delete nh;
//         free((struct publisher_info*) b->private_data);
        ubx_block_unregister(ni, "rosbridge/publisher");
}


UBX_MODULE_INIT(ros_publisher_mod_init)
UBX_MODULE_CLEANUP(ros_publisher_mod_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)
