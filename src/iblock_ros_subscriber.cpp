/*
 * ROS Bridge iblock - Publisher
 * Authors: Enea Scioni, Sebastian Blumenthal
 * email: enea dot scioni at unife dot it
 *        enea dot scioni at kuleuven dot be
 *        sebastian dot blumenthal at kuleuven dot be
 */

#include <ros/ros.h>
#include <std_msgs/ByteMultiArray.h>
#include "ubx.h"
#include <stdio.h>
#include "types/ptrig_config.h" // (still) part of std_blocks in microblox
#include "types/ptrig_config.h.hexarr"

/* hexdump a buffer */
static void hexdump(unsigned char *buf, unsigned long index, unsigned long width)
{
	unsigned long i, fill;

	for (i=0;i<index;i++) { printf("%02x ", buf[i]); } /* dump data */
	for (fill=index; fill<width; fill++) printf(" ");  /* pad on right */

	printf(": ");

	/* add ascii repesentation */
	for (i=0; i<index; i++) {
		if (buf[i] < 32) printf(".");
		else printf("%c", buf[i]);
	}
	printf("\n");
}

struct subscriber_data {
  std_msgs::ByteMultiArray* ros_msg;
  ros::NodeHandle* nh;
  ros::Subscriber* sub;
  ros::AsyncSpinner* spinner;

  /* Single line but huge design impact:
   * The output_buffer holds all the data that is transfered
   * to a caller of the read function (pointer). The intention
   * is to let the interaction block be responsible for managing
   * the data (to follow the design philosophy of microblox)
   * NOTE: An alternative based on a copy by value semantic is
   * difficult to implement as the _size_ of the data is not necessarily
   * known in advance by the caller.
   */
  unsigned char* output_buffer;
  unsigned long output_buffer_size;		/* size in bytes */
  struct ptrig_config* trig_list;			/* optional list of "oberver cblocks" similar as in ptrig block */
  unsigned int trig_list_len;

  subscriber_data()
    : ros_msg(new std_msgs::ByteMultiArray())
  {
  }
};

char subscriber_meta[] =
  "{ doc='ROS bridge subscriber interaction block',"
  "  license='MIT',"
  "  real-time=false,"
  "}";
  
ubx_config_t subscriber_config[] = {
        { .name="topic_name", .type_name = "char" },
        { .name="trig_blocks", .type_name = "struct ptrig_config", .doc = "List of observers that get triggerd on arrival of new data." },
        { NULL },
};

ubx_port_t subscriber_ports[] = {
         { .name="byte_stream", .attrs=PORT_DIR_OUT, .out_type_name="unsigned char" },
        { NULL },
};

static int subscribe_fnc(ubx_block_t *i, ubx_data_t* data) 
{
  struct subscriber_data *inf=(subscriber_data *)i->private_data;
  ERR("subscribe_fnc invoked");

  /*
   * Copy data from buffered ROS message to the ubx data structure
   */

  /* retrieve size of the (last) received ROS message*/
  long int msg_length = inf->ros_msg->layout.dim[0].size;

  /* reserve according internal memory */
  if (inf->output_buffer_size < msg_length) {
	  ERR("Resizing internal ouput buffer with length (in bytes) %li to %li", inf->output_buffer_size, msg_length);
    free(inf->output_buffer);
    inf->output_buffer_size = msg_length;

    if((inf->output_buffer = (unsigned char *)malloc(inf->output_buffer_size)) == NULL) {
      ERR("failed to allocate output buffer");
      free(inf->output_buffer);
      return 0;
    }
  }

  /* copy data (unclear if ROS message is memory aligned or not - but we don't count on it) */
  for (int idx = 0; idx < msg_length; ++idx) {
	  inf->output_buffer[idx] = inf->ros_msg->data[idx];
  }
//  hexdump(inf->output_buffer, inf->output_buffer_size, 16);

  /* setup type */
  ubx_type_t* type =  ubx_type_get(i->ni, "unsigned char");

  /* assemble everything */
  data->data = inf->output_buffer;
  data->len = inf->output_buffer_size;
  data->type = type;
  hexdump((unsigned char*)data->data, data->len, 16);

  return 0;
}

//void subscriber_cb(const std_msgs::ByteMultiArray::ConstPtr& msg)
void subscriber_cb(const std_msgs::ByteMultiArray::ConstPtr& msg, ubx_block_t *i)
{
  ERR("I received something!!! :-)!");
  struct subscriber_data *inf=(subscriber_data *)i->private_data;

  int length = msg->layout.dim[0].size;
  char *buffer = new char [length];
  for (int idx = 0; idx < length; ++idx) {
	  buffer[idx] = msg->data[idx];
  }
  hexdump((unsigned char *)buffer, msg->layout.dim[0].size, 16);

  /* buffer a single ROS message until next one comes */
  inf->ros_msg->layout = msg->layout;
  inf->ros_msg->data = msg->data;

  /* inform potential "observers" */
  for(int idx=0; idx<inf->trig_list_len; idx++) {
	  ubx_cblock_step(inf->trig_list[idx].b);
  }

  ERR("Finished.");
}

extern "C" {
  bool ros_master_check() {
    return ros::master::check();
  }
}

static int subscriber_init(ubx_block_t *b)
{
  int ret=-1;
  if(!ros::isInitialized())
  {
    ERR("ros init");
    int argc=0;
    char ** argv=NULL;
    ros::init(argc,argv,"microblx",ros::init_options::AnonymousName);
    if(ros_master_check())
      ros::start();
    else
    {
      ERR("'roscore' is not running: no ROS functions will be available.");
      ros::shutdown();
      goto out;
    }
  }
  else
    ERR("skip");
  
//   static ros::AsyncSpinner spinner(1); // Use 1 threads
//   spinner.start();
  
    
  char* tname; unsigned int len;
  tname = (char *) ubx_config_get_data_ptr(b, "topic_name", &len);
//   bool isok;
  if(strncmp(tname, "", len)==0) 
  {
    ERR("config topic_name unset");
    goto out;
  }
  
  struct subscriber_data *inf;
  /*allocate internal block data*/
  if((inf=(struct subscriber_data*)calloc(1,sizeof(struct subscriber_data)))==NULL) {
    ERR("failed to allocate subscriber_data");
    goto out;
  }

  b->private_data=inf;
  inf->ros_msg = new std_msgs::ByteMultiArray();
  inf->nh = new ros::NodeHandle();
  inf->spinner = new ros::AsyncSpinner(1);
  inf->sub = new ros::Subscriber(inf->nh->subscribe<std_msgs::ByteMultiArray>(tname, 10, boost::bind(subscriber_cb, _1, b)));
  inf->spinner->start();
  inf->output_buffer = 0;
  inf->output_buffer_size = 0;

  /* possible observers */
  ubx_data_t* trig_list_data;
  trig_list_data = ubx_config_get_data(b, "trig_blocks");
  inf->trig_list = (struct ptrig_config*)trig_list_data->data;
  inf->trig_list_len = (unsigned int)trig_list_data->len;
  ERR("Subsriber iblock has %i observers attached.", inf->trig_list_len);

      ret=0;
      goto out;
// out_free:
//       free(b->private_data);
out:
      return ret;
}

static void subscriber_cleanup(ubx_block_t *b) {
  ERR("cleaningup");
  struct subscriber_data* inf = (struct subscriber_data*)(b->private_data);
  inf->sub->shutdown();
  delete inf->sub;
  delete inf->nh;
  free((struct subscriber_data*) b->private_data);
}

/* put everything together */
ubx_block_t subscriber_comp = {
        .name = "rosbridge/subscriber",
        .type = BLOCK_TYPE_INTERACTION,
        .meta_data = subscriber_meta,
        
        .configs = subscriber_config,
        
        /* ops */
        .init = subscriber_init,
        .read = subscribe_fnc,
//         .write=microblx_sendout,
        .cleanup = subscriber_cleanup,
};

static int subscriber_mod_init(ubx_node_info_t* ni)
{
  return ubx_block_register(ni, &subscriber_comp);
}

static void subscriber_mod_cleanup(ubx_node_info_t *ni)
{
  ubx_block_unregister(ni, "rosbridge/subscriber");
}


UBX_MODULE_INIT(subscriber_mod_init)
UBX_MODULE_CLEANUP(subscriber_mod_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)
